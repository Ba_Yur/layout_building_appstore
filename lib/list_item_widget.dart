import 'package:flutter/material.dart';
import 'package:layout_building_appstore/widget_of_the_day.dart';

class ListItemWidget extends StatefulWidget {
  static String routeName = '/black-hole-prev';

  @override
  _ListItemWidgetState createState() => _ListItemWidgetState();
}

class _ListItemWidgetState extends State<ListItemWidget> {
  String lorem =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend nisl nec lacus molestie, sed placerat lectus mollis. Nam sodales leo et felis pretium congue. Vivamus non elit vitae risus fermentum ultricies id id mi. Quisque ultricies, quam ac sagittis vestibulum, quam nisi euismod purus, non gravida ligula nibh imperdiet sem. Ut eleifend arcu vitae viverra pretium. Etiam vel lorem vel diam auctor blandit. Suspendisse leo justo, sodales eget risus eu, tincidunt lobortis dui. Quisque hendrerit risus eu rutrum pulvinar. Nulla tincidunt, mi consequat dictum auctor, erat ligula posuere massa, a egestas arcu mi non dolor. Etiam quis condimentum elit, commodo malesuada magna.';

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          PageRouteBuilder(
            opaque: false,
            transitionDuration: Duration(milliseconds: 700),
            // settings: ,
            pageBuilder: (context, _, __) {
              return WidgetOfTheDay();
            },
          ),
        );
        // Navigator.of(context).pushNamed(WidgetOfTheDay.routeName);
      },
      child: Material(
        child: Hero(
          tag: 'black_hole',
          // flightShuttleBuilder:
          //     (flightContext, animation, direction, fromContext, toContext) {
          //   Hero toHero = toContext.widget;
          //   return direction == HeroFlightDirection.push
          //       ?
          //   ScaleTransition(
          //           scale: animation.drive(
          //             Tween<double>(begin: 0.75, end: 1.02).chain(
          //               CurveTween(
          //                 curve: Interval(0.4, 1.0, curve: Curves.easeInOut),
          //               ),
          //             ),
          //           ),
          //         )
          //       :
          //   SizeTransition(
          //           sizeFactor: animation,
          //           child: toHero.child,
          //         );
          // },
          child: Container(
            margin: EdgeInsets.symmetric(
              vertical: 10,
            ),
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            width: double.infinity,
            height: 400,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5.0,
                    blurRadius: 10.0,
                    offset: Offset(0, 10.0),
                  )
                ],
                image: DecorationImage(
                    image: NetworkImage(
                        'https://media.wired.com/photos/5a593a7ff11e325008172bc2/master/pass/pulsar-831502910.jpg'),
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(15.0)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 20.0),
                      width: 130,
                      child: Text(
                        'Every friday',
                        style: TextStyle(
                          height: 0.8,
                          fontSize: 50,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.right,
                        maxLines: 3,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 30,
                      padding: EdgeInsets.symmetric(horizontal: 15.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Center(
                        child: Text(
                          '\$4.99',
                          style: TextStyle(color: Colors.blue, fontSize: 18),
                        ),
                      ),
                    ),
                    Spacer(),
                    Container(
                      width: 100,
                      child: Text(
                        'Fly to black holes with us',
                        style: TextStyle(fontSize: 15, color: Colors.white),
                        maxLines: 2,
                        textAlign: TextAlign.right,
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Icon(
                      Icons.face_rounded,
                      color: Colors.white70,
                      size: 40.0,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
