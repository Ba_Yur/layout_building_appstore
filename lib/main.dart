import 'package:flutter/material.dart';
import 'package:layout_building_appstore/widget_of_the_day.dart';

import 'another_item.dart';
import 'list_item_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
      routes: {
        WidgetOfTheDay.routeName: (context) => WidgetOfTheDay(),
        ListItemWidget.routeName: (context) => ListItemWidget(),
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: [

            AnotherItemWidget(),
            ListItemWidget(),
            AnotherItemWidget(),
          ],
        ),
      ),
    );
  }
}
