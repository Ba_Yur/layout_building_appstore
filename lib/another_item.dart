import 'package:flutter/material.dart';
import 'package:layout_building_appstore/widget_of_the_day.dart';

class AnotherItemWidget extends StatelessWidget {

  static String routeName = '/black-hole-prev';

  String lorem =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend nisl nec lacus molestie, sed placerat lectus mollis. Nam sodales leo et felis pretium congue. Vivamus non elit vitae risus fermentum ultricies id id mi. Quisque ultricies, quam ac sagittis vestibulum, quam nisi euismod purus, non gravida ligula nibh imperdiet sem. Ut eleifend arcu vitae viverra pretium. Etiam vel lorem vel diam auctor blandit. Suspendisse leo justo, sodales eget risus eu, tincidunt lobortis dui. Quisque hendrerit risus eu rutrum pulvinar. Nulla tincidunt, mi consequat dictum auctor, erat ligula posuere massa, a egestas arcu mi non dolor. Etiam quis condimentum elit, commodo malesuada magna.';

  @override
  Widget build(BuildContext context) {
    return Material(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(WidgetOfTheDay.routeName);
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          width: double.infinity,
          height: 400,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      'https://lh3.googleusercontent.com/sfMuihSgBo2cwvEJ34gv-n0CmO_nM-BxFo_RW7cB0bxNLcSRnNfQHOibrgK80cRZvLiHPFxyyiLWD-tqQLydImGLCvQ=w640-h400-e365-rj-sc0x00ffffff'),
                  fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(15.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 20.0),
                    width: 130,
                    child: Text(
                      'Every friday',
                      style: TextStyle(
                        height: 0.8,
                        fontSize: 50,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.right,
                      maxLines: 3,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 30,
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Center(
                      child: Text(
                        '\$4.99',
                        style: TextStyle(color: Colors.blue, fontSize: 18),
                      ),
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: 100,
                    child: Text(
                      'Fly to black holes with us',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                      maxLines: 2,
                      textAlign: TextAlign.right,
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(
                    Icons.face_rounded,
                    color: Colors.white70,
                    size: 40.0,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
