import 'dart:async';

import 'package:flutter/material.dart';

const SCALE_ANIMATION_STANDARD = 100;
const POP_STANDARD = 130;

class WidgetOfTheDay extends StatefulWidget {
  static String routeName = '/black-hole-full';

  @override
  _WidgetOfTheDayState createState() => _WidgetOfTheDayState();
}

class _WidgetOfTheDayState extends State<WidgetOfTheDay> with TickerProviderStateMixin {
  String lorem =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend nisl nec lacus molestie, sed placerat lectus mollis. Nam sodales leo et felis pretium congue. Vivamus non elit vitae risus fermentum ultricies id id mi. Quisque ultricies, quam ac sagittis vestibulum, quam nisi euismod purus, non gravida ligula nibh imperdiet sem. Ut eleifend arcu vitae viverra pretium. Etiam vel lorem vel diam auctor blandit. Suspendisse leo justo, sodales eget risus eu, tincidunt lobortis dui. Quisque hendrerit risus eu rutrum pulvinar. Nulla tincidunt, mi consequat dictum auctor, erat ligula posuere massa, a egestas arcu mi non dolor. Etiam quis condimentum elit, commodo malesuada magnaLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend nisl nec lacus molestie, sed placerat lectus mollis. Nam sodales leo et felis pretium congue. Vivamus non elit vitae risus fermentum ultricies id id mi. Quisque ultricies, quam ac sagittis vestibulum, quam nisi euismod purus, non gravida ligula nibh imperdiet sem. Ut eleifend arcu vitae viverra pretium. Etiam vel lorem vel diam auctor blandit. Suspendisse leo justo, sodales eget risus eu, tincidunt lobortis dui. Quisque hendrerit risus eu rutrum pulvinar. Nulla tincidunt, mi consequat dictum auctor, erat ligula posuere massa, a egestas arcu mi non dolor. Etiam quis condimentum elit, commodo malesuada magnaLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend nisl nec lacus molestie, sed placerat lectus mollis. Nam sodales leo et felis pretium congue. Vivamus non elit vitae risus fermentum ultricies id id mi. Quisque ultricies, quam ac sagittis vestibulum, quam nisi euismod purus, non gravida ligula nibh imperdiet sem. Ut eleifend arcu vitae viverra pretium. Etiam vel lorem vel diam auctor blandit. Suspendisse leo justo, sodales eget risus eu, tincidunt lobortis dui. Quisque hendrerit risus eu rutrum pulvinar. Nulla tincidunt, mi consequat dictum auctor, erat ligula posuere massa, a egestas arcu mi non dolor. Etiam quis condimentum elit, commodo malesuada magnaLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend nisl nec lacus molestie, sed placerat lectus mollis. Nam sodales leo et felis pretium congue. Vivamus non elit vitae risus fermentum ultricies id id mi. Quisque ultricies, quam ac sagittis vestibulum, quam nisi euismod purus, non gravida ligula nibh imperdiet sem. Ut eleifend arcu vitae viverra pretium. Etiam vel lorem vel diam auctor blandit. Suspendisse leo justo, sodales eget risus eu, tincidunt lobortis dui. Quisque hendrerit risus eu rutrum pulvinar. Nulla tincidunt, mi consequat dictum auctor, erat ligula posuere massa, a egestas arcu mi non dolor. Etiam quis condimentum elit, commodo malesuada maLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend nisl nec lacus molestie, sed placerat lectus mollis. Nam sodales leo et felis pretium congue. Vivamus non elit vitae risus fermentum ultricies id id mi. Quisque ultricies, quam ac sagittis vestibulum, quam nisi euismod purus, non gravida ligula nibh imperdiet sem. Ut eleifend arcu vitae viverra pretium. Etiam vel lorem vel diam auctor blandit. Suspendisse leo justo, sodales eget risus eu, tincidunt lobortis dui. Quisque hendrerit risus eu rutrum pulvinar. Nulla tincidunt, mi consequat dictum auctor, erat ligula posuere massa, a egestas arcu mi non dolor. Etiam quis condimentum elit, commodo malesuada magnagna.';

  AnimationController _heightController;

  Animation _heightAnimation;

  Animation _heightBackAnimation;

  AnimationController _closeController;

  Animation _closeAnimation;

  double _initPoint;

  double _verticalDistance;

  bool _needPop;

  bool _isTop;

  bool _opacity;

  @override
  void initState() {
    _needPop = true;
    _isTop = true;
    _opacity = false;

    _heightController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _closeController = AnimationController(vsync: this, duration: Duration(milliseconds: 400));

    _closeAnimation = Tween<double>(begin: 1.0, end: 0.75).animate(_closeController);

    _heightAnimation = Tween<double>(begin: 0.9, end: 1).animate(CurvedAnimation(curve: Curves.easeIn, parent: _heightController));

    _heightBackAnimation = Tween<double>(begin: 0.6, end: 1).animate(CurvedAnimation(curve: Curves.easeIn, parent: _heightController));

    super.initState();

    _heightController.forward();

    Timer(Duration(milliseconds: 250), () {
      setState(() {
        _opacity = true;
      });
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _closeController.dispose();
    _heightController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.85),
      body: AnimatedBuilder(
        animation: _closeAnimation,
        builder: (contex, _child) {
          // only trigger at router pop.
          return Transform.scale(
            scale: _closeAnimation.value,
            child: _child,
          );
        },
        child: AnimatedOpacity(
          opacity: _opacity ? 1 : 0,
          duration: Duration(milliseconds: 150),
          // Controls Container height.
          child: SizeTransition(
            sizeFactor: _needPop ? _heightAnimation : _heightBackAnimation,
            child: Container(
              width: double.infinity,
              constraints: BoxConstraints(
                minHeight: 300,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10), topRight: Radius.circular(10), bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 10,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Listener(
                onPointerDown: (opm) {
                  _initPoint = opm.position.dy;
                },
                onPointerUp: (opm) {
                  if (_needPop) {
                    _closeController.reverse();
                  }
                },
                onPointerMove: (opm) {
                  _verticalDistance = -_initPoint + opm.position.dy;
                  if (_verticalDistance >= 0) {
                    // scroll up
                    if (_isTop == true && _verticalDistance < SCALE_ANIMATION_STANDARD) {
                      double _scaleValue = double.parse((_verticalDistance / 100).toStringAsFixed(2));
                      _closeController.animateTo(_scaleValue, duration: Duration(milliseconds: 0), curve: Curves.linear);
                    } else if (_isTop == true && _verticalDistance >= SCALE_ANIMATION_STANDARD && _verticalDistance < POP_STANDARD) {
                      // stop animation
                      _closeController.animateTo(1, duration: Duration(milliseconds: 0), curve: Curves.linear);
                    } else if (_isTop == true && _verticalDistance >= POP_STANDARD) {
                      if (_needPop) {
                        // pop
                        _needPop = false;
                        _closeController.fling(velocity: 1).then((_) {
                          _heightController.reverse();
                          Navigator.of(context).pop();
                          _opacity = false;
                        });
                      }
                    }
                  } else {
                    _isTop = false;
                  }
                },
                child: NotificationListener<ScrollNotification>(
                  onNotification: (scrollNotification) {
                    // scroll update function
                    if (scrollNotification is ScrollUpdateNotification) {
                      double scrollDistance = scrollNotification.metrics.pixels;
                      if (scrollDistance <= 3) {
                        _isTop = true;
                      }
                    }
                    return true;
                  },
                  child: CustomScrollView(
                    slivers: <Widget>[
                      SliverAppBar(
                        expandedHeight: 300,
                        // hide the back button
                        leading: IconButton(
                          icon: Icon(
                            Icons.close,
                            size: 30,
                            color: Colors.grey,
                          ),
                          onPressed: (){
                            Navigator.of(context).pop();
                          },
                        ),
                        backgroundColor: Colors.white,
                        flexibleSpace: FlexibleSpaceBar(
                          background: Hero(
                            tag: 'black_hole',
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                              width: double.infinity,
                              height: 300,
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5.0,
                                      blurRadius: 10.0,
                                      offset: Offset(0, 10.0),
                                    )
                                  ],
                                  image: DecorationImage(
                                      image: NetworkImage('https://media.wired.com/photos/5a593a7ff11e325008172bc2/master/pass/pulsar-831502910.jpg'),
                                      fit: BoxFit.cover),
                                  borderRadius: BorderRadius.circular(15.0)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Spacer(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(bottom: 20.0),
                                        width: 130,
                                        child: Text(
                                          'Every friday',
                                          style: TextStyle(
                                            height: 0.8,
                                            fontSize: 50,
                                            color: Colors.white,
                                          ),
                                          textAlign: TextAlign.right,
                                          maxLines: 3,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 30,
                                        padding: EdgeInsets.symmetric(horizontal: 15.0),
                                        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(15.0)),
                                        child: Center(
                                          child: Text(
                                            '\$4.99',
                                            style: TextStyle(color: Colors.blue, fontSize: 18),
                                          ),
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        width: 100,
                                        child: Text(
                                          'Fly to black holes with us',
                                          style: TextStyle(fontSize: 15, color: Colors.white),
                                          maxLines: 2,
                                          textAlign: TextAlign.right,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Icon(
                                        Icons.face_rounded,
                                        color: Colors.white70,
                                        size: 40.0,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate(
                          [
                            Container(
                              padding: EdgeInsets.only(left: 20, right: 20, top: 50, bottom: 30),
                              width: double.infinity,
                              child: Column(
                                children: <Widget>[
                                  Text('Title', style: TextStyle(fontSize: 18)),
                                  SizedBox(height: 30),
                                  Text(lorem,
                                      style: TextStyle(
                                        fontSize: 15,
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
